﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Quiz.Data;
using Quiz.Models;

namespace Quiz.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GamesApiController : ControllerBase
    {
        private GameRepository _gameRepository = new GameRepository();

        public GamesApiController(QuizContext context)
        {

        }

        // GET: api/GamesApi
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Game>>> GetGames()
        {
            var games =  await _gameRepository.GetItems(null);
            return games.ToList();
        }

        // GET: api/GamesApi/5
        [HttpGet("GetGame/{id}")]
        public ActionResult<Game> GetGame(string id)
        {
            var game = GetGames().Result.Value.FirstOrDefault(g => g.GameId == id);

            if (game == null)
            {
                return NotFound();
            }

            return game;
        }

        // GET: api/GamesApi/5
        [HttpGet("GetGameByName/{name}")]
        public ActionResult<Game> GetGameByName(string name)
        {
            var game = GetGames().Result.Value.FirstOrDefault(g => g.Name == name);

            if (game == null)
            {
                return NotFound();
            }

            return game;
        }
    }
}
