﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Quiz.Data;
using Quiz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoundsApiController : ControllerBase
    {
        private readonly QuizContext _context;

        public RoundsApiController(QuizContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        // GET: api/RoundsApi/5
        [HttpGet("{gameId}")]
        public IEnumerable<Round> GetRounds(string gameId)
        {
            return _context.Rounds.Where(x => x.GameId == gameId).ToList();
        }

        // GET api/RoundsApi/5
        [HttpGet("{id}")]
        public ActionResult<Round> GetRound(int id)
        {
            var round = _context.Rounds.Where(x => x.RoundId == id).FirstOrDefault();

            if (round == null)
            {
                return NotFound();
            }

            return round;
        }

        // POST api/RoundsApi
        [HttpPost]
        public async Task<ActionResult<Round>> PostGame(Round round)
        {
            _context.Rounds.Add(round);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRound", new { id = round.RoundId }, round);
        }

        // PUT api/RoundsApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRound(int id, Round round)
        {
            if (id != round.RoundId)
            {
                return BadRequest();
            }

            _context.Entry(round).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (RoundExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // DELETE: api/RoundsApi/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRound(long id)
        {
            var round = await _context.Rounds.FindAsync(id);
            if (round == null)
            {
                return NotFound();
            }

            _context.Rounds.Remove(round);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool RoundExists(int id)
        {
            return _context.Rounds.Any(e => e.RoundId == id);
        }
    }
    
}

