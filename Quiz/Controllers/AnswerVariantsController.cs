﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models.Answers;
using Quiz.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz.Controllers
{
    public class AnswerVariantsController : Controller
    {
        private readonly GameRepository _gameRepository = new GameRepository();

        public AnswerVariantsController()
        {
        }

        // GET: Questions/Create
        public IActionResult Create(string Id)
        {
            ViewBag.QuestionId = Id.Split('&')[2];
            ViewBag.RoundId = Id.Split('&')[1];
            ViewBag.GameId = Id.Split('&')[0];
            return View();
        }


        // POST: Questions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AnswerVariantId,Variant,QuestionId")] AnswerVariant answerVariant)
        {
            if (ModelState.IsValid)
            {
                var game = await _gameRepository.GetItem(answerVariant.GameId);
                var round = game.Rounds.Where(x => x.RoundId == answerVariant.RoundId).FirstOrDefault();
                var question = round.Questions[answerVariant.QuestionId];
                if (question != null)
                {
                    if (question.AnswerVariants == null)
                        question.AnswerVariants = new List<AnswerVariant>();
                    question.AnswerVariants.Add(answerVariant);
                    var index = question.AnswerVariants.IndexOf(answerVariant);
                    answerVariant.AnswerVariantId = index;
                    await _gameRepository.Update(game);
                }

                return RedirectToRoute( new { controller = "Questions", action = "Edit", id = $"{answerVariant.GameId}&{answerVariant.RoundId}&{answerVariant.QuestionId}" });
            }
            return View(answerVariant);
        }
    }
}
