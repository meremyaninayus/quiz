﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Quiz.Data;
using Quiz.Models;
using System.Collections.Generic;


namespace Quiz.Controllers
{
    public class GamesController : Controller
    {
        private GameRepository _gameContext = new GameRepository();


        public GamesController()
        {
        }

        // GET: Games
        public async Task<IActionResult> Index()
        {
            var games = await _gameContext.GetItems(null);
            return View(games);
        }

        // GET: Games/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var game = await _gameContext.GetItem(id);
            if (game == null)
            {
                return NotFound();
            }

            return View(game);
        }

        // GET: Games/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Games/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GameId,Name")] Game game)
        {
            if (ModelState.IsValid)
            {
                game.Rounds = new List<Round>();
                await _gameContext.Create(game);
                return RedirectToAction(nameof(Edit), new { id = game.GameId });
            }
            return View(game);
        }

        // GET: Games/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var game = await _gameContext.GetItem(id);
            if (game == null)
            {
                return NotFound();
            }
            return View(game);
        }

        // POST: Games/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("GameId,Name")] Game game)
        {
            if (id != game.GameId)
            {
                return NotFound();
            }
            var existingGame = await _gameContext.GetItem(game.GameId);
            existingGame.Name = game.Name;
            if (ModelState.IsValid)
            {
                try
                {
                    await _gameContext.Update(existingGame);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await GameExists(game.GameId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(game);
        }

        // GET: Games/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var game = await _gameContext.GetItem(id);
            if (game == null)
            {
                return NotFound();
            }

            return View(game);
        }

        // POST: Games/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            await _gameContext.Remove(id);
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> GameExists(string id)
        {
            return (await _gameContext.GetItems(id)).Count() > 0;
        }
    }
}
