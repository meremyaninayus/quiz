﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models.Answers;
using Quiz.Data;
using Quiz.Models;

namespace Quiz.Controllers
{
    public class QuestionsController : Controller
    {
        private GameRepository _gameRepository = new GameRepository();

        public QuestionsController()
        {
            //_context = context;
        }



        // GET: Questions/Details/5
        public async Task<IActionResult> Details(string id)
        {
            var ids = id.Split('&');
            var gameId = ids[0];
            var roundId = int.Parse(ids[1]);
            var questionId = int.Parse(ids[2]);
            var game = await _gameRepository.GetItem(gameId);
            var question = game.Rounds[roundId].Questions[questionId];
            if (question == null)
            {
                return NotFound();
            }

            return View(question);
        }

        // GET: Questions/Create
        public IActionResult Create(string Id)
        {
            ViewBag.RoundId = Id.Split('&')[1];
            ViewBag.GameId = Id.Split('&')[0];
            return View();
        }


        // POST: Questions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("QuestionId,QuestionBody,RightAnswer,MaxPoints,AnswerType,RoundId,AnswerVariants,GameId")] Question question)
        {
            if (ModelState.IsValid)
            {
                var game = await _gameRepository.GetItem(question.GameId);
                var round = game.Rounds.Where(x => x.RoundId == question.RoundId).FirstOrDefault();
                if (round != null)
                {
                    if (round.Questions == null)
                        round.Questions = new List<Question>();
                    round.Questions.Add(question);
                    var index = round.Questions.IndexOf(question);
                    question.QuestionId = index;
                }
                await _gameRepository.Update(game);
                return RedirectToRoute(new { controller = "Rounds", action = "Edit", id = $"{round.GameId}&{round.RoundId}" });
            }
            return View(question);
        }

        // GET: Questions/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            var ids = id.Split('&');
            var gameId = ids[0];
            var roundId = int.Parse(ids[1]);
            var questionId = int.Parse(ids[2]);
            var game = await _gameRepository.GetItem(gameId);
            var question =game.Rounds[roundId].Questions[questionId];

            if (question == null)
            {
                return NotFound();
            }
            return View(question);
        }

        // POST: Questions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("QuestionId,QuestionBody,RightAnswer,MaxPoints,AnswerType,AnswerVariants")] Question question)
        {
            var ids = id.Split('&');
            var gameId = ids[0];
            var roundId = int.Parse(ids[1]);
            var questionId = int.Parse(ids[2]);
            var game = await _gameRepository.GetItem(gameId);
            var round = game.Rounds[roundId];
            round.Questions[questionId] = question;
            if (ModelState.IsValid)
            {
                await _gameRepository.Update(game);
                return RedirectToRoute(new { controller = "Rounds", action = "Edit", id = $"{round.GameId}&{round.RoundId}" });
            }
            return View(question);
        }

        // GET: Questions/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            var ids = id.Split('&');
            var gameId = ids[0];
            var roundId = int.Parse(ids[1]);
            var questionId = int.Parse(ids[2]);
            var game = await _gameRepository.GetItem(gameId);
            var question = game.Rounds[roundId].Questions[questionId];
            if (question == null)
            {
                return NotFound();
            }

            return View(question);
        }

        // POST: Questions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var ids = id.Split('&');
            var gameId = ids[0];
            var roundId = int.Parse(ids[1]);
            var questionId = int.Parse(ids[2]);
            var game = await _gameRepository.GetItem(gameId);
            var round = game.Rounds[roundId];
            round.Questions.RemoveAt(questionId);
            await _gameRepository.Update(game);
            return RedirectToRoute(new { controller = "Rounds", action = "Edit", id = $"{round.GameId}&{round.RoundId}" });
        }
    }
}
