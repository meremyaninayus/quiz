﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using Quiz.Data;
using Quiz.Models;

namespace Quiz.Controllers
{
    public class RoundsController : Controller
    {
        private GameRepository _gameRepository = new GameRepository();
        public RoundsController()
        {
        }

        // GET: Rounds/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var ids = id.Split('&');
            var gameId = ids[0];
            var roundId = int.Parse(ids[1]);
            var game = await _gameRepository.GetItem(gameId);
            var round = game.Rounds[roundId];
            if (round == null)
            {
                return NotFound();
            }

            return View(round);
        }

        // GET: Rounds/Create
        public IActionResult Create(string Id)
        {
            ViewBag.GameId = Id;
            return View();
        }

        // POST: Rounds/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Theme,RoundId,GameId")] Round round)
        {
            if (ModelState.IsValid)
            {
                var game = await _gameRepository.GetItem(round.GameId);
                game.Rounds.Add(round);
                var index = game.Rounds.IndexOf(round);
                round.RoundId = index;
                round.Questions = new List<Question>();
                await _gameRepository.Update(game);
                return RedirectToAction(nameof(Edit), new { id = $"{round.GameId}&{round.RoundId}" });
            }
            return View(round);
        }

        // GET: Rounds/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var ids = id.Split('&');
            var gameId = ids[0];
            var roundId = int.Parse(ids[1]);
            var game = await _gameRepository.GetItem(gameId);
            var round = game.Rounds[roundId];
            if (round == null)
            {
                return NotFound();
            }
            return View(round);
        }

        // POST: Rounds/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Theme,RoundId")] Round round)
        {
            var ids = id.Split('&');
            var gameId = ids[0];
            var roundId = int.Parse(ids[1]);
            var game = await _gameRepository.GetItem(gameId);
            round.GameId = gameId;
            game.Rounds[roundId].Theme = round.Theme;
            if (ModelState.IsValid)
            {
                await _gameRepository.Update(game);
                return RedirectToRoute(new { controller = "Games", action = "Edit", id = gameId });
            }
            return View(round);
        }

        // GET: Rounds/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var ids = id.Split('&');
            var gameId = ids[0];
            var roundId = int.Parse(ids[1]);

            var game = await _gameRepository.GetItem(gameId);
            var round = game.Rounds[roundId];
            if (round == null)
            {
                return NotFound();
            }

            return View(round);
        }

        // POST: Rounds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var ids = id.Split('_');
            var gameId = ids[0];
            var roundId = int.Parse(ids[1]);
            var game = await _gameRepository.GetItem(gameId);
            game.Rounds.RemoveAt(roundId);
            await _gameRepository.Update(game);
            return RedirectToRoute(new { controller = "Games", action = "Edit", id = gameId });
        }

    }
}
