﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Quiz.Data;
using Quiz.Models.Messages;

namespace Quiz.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnswerApiController : ControllerBase
    {
        private GameRepository _gameRepository = new GameRepository();

        public AnswerApiController(QuizContext context)
        {

        }

        [HttpPost]
        public ActionResult<VerifyAnswerResponse> Verify(VerifyAnswerRequest request)
        {
            var response = new VerifyAnswerResponse();

            var game = _gameRepository.GetItem(request.GameId).GetAwaiter().GetResult();

            if (game is null)
            {
                return response;
            }

            var round = game.Rounds[request.RoundId];

            if (round is null)
            {
                return response;
            }

            var question = round.Questions[request.QuestionId];

            if (question is null)
            {
                return response;
            }

            response.IsSuccessful = true;
            response.Result = request.Answer == question.RightAnswer;
            return response;
        }
    }
}
