﻿using MongoDB.Bson;
using MongoDB.Driver;
using Quiz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz.Data
{
    public class GameRepository:BaseRepository<Game>
    {


        public GameRepository():base()
        {
        }

        // обращаемся к коллекции Games
        public override IMongoCollection<Game> Items
        {
            get { return _database.GetCollection<Game>("Games"); }
        }

        public override async Task<IEnumerable<Game>> GetItems(string id)
        {
            // строитель фильтров
            var builder = new FilterDefinitionBuilder<Game>();
            var filter = builder.Empty; // фильтр для выборки всех документов
            // фильтр по id

            if (!string.IsNullOrWhiteSpace(id))  
            {
                filter = filter & builder.Eq("GameId", id);
            }

            return await Items.Find(filter).ToListAsync();
        }

        public async Task<Game> GetGameByName(string name)
        {
            // строитель фильтров
            var builder = new FilterDefinitionBuilder<Game>();
            var filter = builder.Empty; // фильтр для выборки всех документов
            // фильтр по id

            if (!string.IsNullOrWhiteSpace(name))
            {
                filter = filter & builder.Eq("Name", name);
            }

            return await Items.Find(filter).FirstOrDefaultAsync();
        }

        // обновление документа
        public override async Task Update(Game c)
        {
            await Items.ReplaceOneAsync(new BsonDocument("_id", new ObjectId(c.GameId.ToString())), c);
        }
    }
}
