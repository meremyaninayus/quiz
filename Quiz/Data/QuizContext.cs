﻿using Microsoft.EntityFrameworkCore;
using Models.Answers;
using Quiz.Models;

namespace Quiz.Data
{
    public class QuizContext : DbContext
    { 
        public QuizContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Question> Questions { get; set; }

        public DbSet<Game> Games { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Round> Rounds { get; set; }

        public DbSet<AnswerVariant> AnswerVariants { get; set; }
    }
}