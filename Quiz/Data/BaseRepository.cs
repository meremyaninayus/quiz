﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz.Data
{
    public abstract class BaseRepository<T>
    {
        //const string connectionString = "mongodb+srv://user:user@cluster0.qcuhp.mongodb.net/Quiz?retryWrites=true&w=majority";
        const string connectionString = "mongodb://127.0.0.1:27017";
        internal readonly IMongoDatabase _database; // база данных

        public BaseRepository()
        {
            var client = new MongoClient(connectionString);
            _database = client.GetDatabase("Quiz");
        }

        // обращаемся к коллекции Games
        public abstract IMongoCollection<T> Items { get; }

        public abstract Task<IEnumerable<T>> GetItems(string id);

        // добавление документа
        public async Task Create(T c)
        {
            await Items.InsertOneAsync(c);
        }

        // обновление документа
        public abstract Task Update(T c);


        // получаем один документ по id
        public async Task<T> GetItem(string id)
        {
            return await Items.Find(new BsonDocument("_id", new ObjectId(id.ToString()))).FirstOrDefaultAsync();
        }

        // удаление документа
        public async Task Remove(string id)
        {
            await Items.DeleteOneAsync(new BsonDocument("_id", new ObjectId(id.ToString())));
        }
    }
}

