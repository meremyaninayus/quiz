﻿using MongoDB.Bson;
using MongoDB.Driver;
using Quiz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz.Data
{
    public class UserRepository : BaseRepository<User>
    {
        public override IMongoCollection<User> Items
        {
            get { return _database.GetCollection<User>("Users"); }
        }

        public override async Task<IEnumerable<User>> GetItems(string id)
        {
            // строитель фильтров
            var builder = new FilterDefinitionBuilder<User>();
            var filter = builder.Empty; // фильтр для выборки всех документов
            // фильтр по id

            if (!string.IsNullOrWhiteSpace(id))
            {
                filter = filter & builder.Eq("UserId", id);
            }

            return await Items.Find(filter).ToListAsync();
        }

        public override async Task Update(User c)
        {
            await Items.ReplaceOneAsync(new BsonDocument("_id", new ObjectId(c.UserId.ToString())), c);
        }
    }
}
