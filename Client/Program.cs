﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Quiz.Models;
using Quiz.Models.Messages;
using RestSharp;

namespace Client
{
    static class Program
    {
        private const string BaseAddress = "https://localhost:44365/api";

        static void Main(string[] args)
        {
            Console.WriteLine("You wanna play? Let's play!");
            Console.WriteLine("Enter game name");

            var gameName = Console.ReadLine();
            var game = GetGame(gameName);

            Console.WriteLine($"You are starting your {game.Name} game");

            var responses = new List<VerifyAnswerResponse>();
            foreach (var round in game.Rounds)
            {
                Console.WriteLine(round.Theme);
                foreach (var question in round.Questions)
                {
                    Console.WriteLine(question.QuestionBody);
                    var answer = Console.ReadLine();

                    var verifyResponse = VerifyAnswer(
                        new VerifyAnswerRequest
                        {
                            GameId = game.GameId,
                            RoundId = round.RoundId,
                            QuestionId = question.QuestionId,
                            Answer = answer
                        });
                    responses.Add(verifyResponse);

                    var isRightAnswer = verifyResponse.Result.Value ? "right" : "wrooooong";

                    Console.WriteLine($"Your answer is {isRightAnswer}");
                }
                Console.WriteLine("Round is over");
            }

            Console.WriteLine(
                $"Well done! You gave {responses.Count(r => r.Result.Value)} right answers for {responses.Count} questions");
        }

        private static Game GetGame(string gameName)
        {
            var responseContent = SendRequest(Method.GET, $"/gamesApi/GetGameByName/{gameName}", null);
            return JsonConvert.DeserializeObject<Game>(responseContent);
        }


        private static VerifyAnswerResponse VerifyAnswer(VerifyAnswerRequest verifyAnswerRequest)
        {
            var responseContent = SendRequest(Method.POST, "/AnswerApi", verifyAnswerRequest);
            return JsonConvert.DeserializeObject<VerifyAnswerResponse>(responseContent);
        }

        private static string SendRequest(Method method, string uri, object requestBody)
        {
            var request = new RestRequest(uri, method);
            request.AddParameter("application/json; charset=utf-8", JsonConvert.SerializeObject(requestBody), ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            var client = new RestClient(BaseAddress);
            var response = client.ExecuteAsync(request).Result;

            return response.Content;
        }
    }
}
