﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quiz.Models
{
    public class User
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public UserRole Role { get; set; }
        public string Email { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        [BsonId]
        public string UserId { get; set; }
    }

    public enum UserRole
    {
        User,
        Admin,
        SuperAdmin
    }
}
