﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Models.Answers;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Quiz.Models.Dictionaries;

namespace Quiz.Models
{
    public class Question
    {
        public int RoundId { get; set; }
        
        public int QuestionId { get; set; }

        public string QuestionBody { get; set; }

        public string RightAnswer { get; set; }
        
        public int MaxPoints { get; set; }

        [EnumDataType(typeof(AnswerType))]
        public AnswerType AnswerType { get; set; }

        public List<AnswerVariant> AnswerVariants { get; set; }

        public string GameId { get; set; }
    }

}
