﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System.Collections.Generic;


namespace Quiz.Models
{
    public class Round
    {
        public string Theme { get; set; }

        public List<Question> Questions { get; set; }

        public int RoundId { get; set; }

        public string GameId { get; set; }
    }
}
