﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace Quiz.Models
{
    [BsonIgnoreExtraElements]
    public class Game
    {
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonId]
        public string GameId { get; set; }

        public string Name { get; set; }

        public List<Round> Rounds { get; set; }
    }
}
