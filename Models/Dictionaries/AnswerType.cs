﻿namespace Quiz.Models.Dictionaries
{
    public enum AnswerType
    {
        SelectOne,
        ClosestNumber,
        OpenAnswer
    }
}