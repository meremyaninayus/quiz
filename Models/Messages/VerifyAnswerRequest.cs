﻿namespace Quiz.Models.Messages
{
    public class VerifyAnswerRequest
    {
        public string GameId { get; set; }

        public int QuestionId { get; set; }

        public int RoundId { get; set; }

        public string Answer { get; set; }
    }
}
