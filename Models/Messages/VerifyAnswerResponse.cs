﻿namespace Quiz.Models.Messages
{
    public class VerifyAnswerResponse : BaseResponse
    {
        public bool? Result { get; set; }
    }
}
