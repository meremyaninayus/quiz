﻿namespace Quiz.Models.Messages
{
    public abstract class BaseResponse
    {
        public bool IsSuccessful { get; set; }
    }
}
