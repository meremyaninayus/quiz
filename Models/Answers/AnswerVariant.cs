﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Answers
{
    public class AnswerVariant
    {
        public int AnswerVariantId { get; set; }
        public string Variant { get; set; }

        public int QuestionId { get; set; }

        public int RoundId { get; set; }

        public string GameId { get; set; }
    }
}
