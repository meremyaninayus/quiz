﻿using System;
using Quiz.Models.Dictionaries;

namespace Models.Answers
{
    public class StringAnswer : Answer
    {
        private readonly string _rightAnswer;
        
        public StringAnswer(int maxPoints, string rightAnswer) : base(maxPoints, AnswerType.OpenAnswer)
        {
            _rightAnswer = rightAnswer;
        }

        public override int ValidateAnswer(string answer) => 
            answer.Equals(_rightAnswer, StringComparison.InvariantCultureIgnoreCase) 
                ? _maxPoints 
                : 0;
    }
}