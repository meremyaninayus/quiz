﻿using Quiz.Models;
using Quiz.Models.Dictionaries;

namespace Models.Answers
{
    public class ClosestNumberAnswer : Answer
    {
        private readonly int _rightAnswer;
        
        public ClosestNumberAnswer(int maxPoints, int rightAnswer) : base(maxPoints, AnswerType.ClosestNumber)
        {
            _rightAnswer = rightAnswer;
        }

        public override int ValidateAnswer(string answer)
        {
            if (!int.TryParse(answer, out var intResult))
            {
                return 0;
            }

            decimal ratio = intResult / _rightAnswer;
            return ratio switch
            {
                1 => _maxPoints,
                // TODO Add logic
                _ => 0
            };

        }
    }
}