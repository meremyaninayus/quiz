﻿using System;
using Quiz.Models.Dictionaries;

namespace Models.Answers
{
    public class ClosestNumberRightAnswer : RightAnswer
    {
        private readonly int _intRightAnswer;
        
        public ClosestNumberRightAnswer(int maxPoints, string rightAnswer) 
            : base(AnswerType.ClosestNumber, maxPoints, rightAnswer)
        {
            if (!int.TryParse(rightAnswer, out _intRightAnswer))
            {
                throw new ArgumentException();
            }
        }

        public override int ValidateAnswer(string answer)
        {
            if (!int.TryParse(answer, out var intAnswer))
            {
                throw new ArgumentException();
            }
            decimal ratio = intAnswer / _intRightAnswer;
            return ratio switch
            {
                1 => _maxPoints,
                // TODO Add logic
                _ => 0
            };

        }
    }
}