﻿using Quiz.Models.Dictionaries;

namespace Models.Answers
{
    public abstract class Answer
    {
        protected readonly int _maxPoints;

        private AnswerType _answerType;

        protected Answer(int maxPoints, AnswerType answerType)
        {
            _maxPoints = maxPoints;
            _answerType = answerType;
        }
        
        public abstract int ValidateAnswer(string answer);
    }
}