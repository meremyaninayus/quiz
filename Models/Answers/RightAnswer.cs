﻿using Quiz.Models.Dictionaries;

namespace Models.Answers
{
    public abstract class RightAnswer
    {
        protected readonly int _maxPoints;

        protected readonly string _rightAnswer;

        private AnswerType _answerType;

        protected RightAnswer(AnswerType answerType, int maxPoints, string rightAnswer)
        {
            _maxPoints = maxPoints;
            _answerType = answerType;
            _rightAnswer = rightAnswer;
        }
        
        public abstract int ValidateAnswer(string answer);
    }
}