﻿using System;
using Quiz.Models.Dictionaries;

namespace Models.Answers
{
    public class StringRightAnswer : RightAnswer
    {
        public StringRightAnswer(int maxPoints, string rightAnswer) : base(AnswerType.OpenAnswer, maxPoints, rightAnswer)
        {
        }

        public override int ValidateAnswer(string answer) => 
            answer.Equals(_rightAnswer, StringComparison.InvariantCultureIgnoreCase) 
                ? _maxPoints 
                : 0;
    }
}